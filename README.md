Technologie employée dans le projet
===================================

Ce projet est développé avec Angular 13, les sources se trouve dans un répertoire public gitlab : <https://gitlab.com/benjamin.ruotte/2510crowdsourcing>

De plus ce projet utilise en backend un serveur Apache2 pour héberger une api développée en php et un serveur Mysql. Tous ces services sont compilés et déployés dans un cluster de containers docker. L'orchestration des containers est assurée par docker-compose.

Comment installer et déployer l'application 2510 crowdsourcing
==============================================================

Cette documentation se concentre sur une installation sous Windows comme notre environnement de test, pour linux il faut suivre le processus d'installation en lien [ici](https://docs.docker.com/engine/install/ubuntu/).

Activation de WSL 2 et installation d'un kernel linux
-----------------------------------------------------

1.  ouvrir un terminal Windows en administrateur et taper la commande suivante :

1.  téléchargez le package de mise à jour du noyau linux [ici](https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi)
2.  redémarrer votre ordinateur
3.  définir WSL 2 comme version par défaut :

1.  dans le Microsoft store, télécharger une distribution linux (nous choisirons [Ubuntu 20.04 LTS](https://www.microsoft.com/store/apps/9n6svws3rx71))

Installation de Docker desktop
------------------------------

1.  Télécharger Docker [ici](https://desktop.docker.com/win/main/amd64/Docker%20Desktop%20Installer.exe)
2.  Lancer l'exécutable Docker et suivre les instructions de l'installateur
3.  une fois l'installation terminée, lancer Docker Desktop
4.  dans les paramètres cocher la case "Use the WSL 2 based engine"

1.  cliquer sur "Apply & Restart"

Installation de git
-------------------

Ouvrez votre terminal wsl

Vérifiez si git est déjà installé grâce à la commande suivante:


Si git n'est pas reconnu comme en tant que commande il faut l'installer avec la commande

Pour simplifier l'utilisation de git il est recommandé de configurer le compte par défaut

Entrez le nom de votre compte gitlab

Entrez ensuite l'adresse associé

Le git est maintenant prêt

Télécharger les sources depuis git
----------------------------------

Pour télécharger les sources, entrez dans wsl et à l'emplacement souhaité dans votre système de fichier entrez la commande suivante:

*git clone https://gitlab.com/benjamin.ruotte/2510crowdsourcing.git*

Déploiement de l'application
----------------------------

Maintenant que les sources sont téléchargés, nous pouvons lancer les services depuis un container docker avec la commande docker-compose:

*docker-compose up -f "chemin/vers/docker-compose.yaml" --build*

vous devriez voir les logs des services s'afficher avec 4 services différents

*2510crownsourcin_app*

*2510crownsourcin_www*

*2510crownsourcin_db*

*2510crownsourcin_phpmyadmin*

Si tous les services sont lancés sans erreur, vous devez pouvoir accéder au service avec votre navigateur.

[*http://localhost:80*](http://localhost/) *pour l'applicaiton 2510crowndsouring*

[*http://localhost:8001*](http://localhost:8001/) *pour l'api*

[*http://localhost:3306*](http://localhost:3306/) *pour mysql*

[*http://localhost:8000*](http://localhost:8000/) *pour phpmyadmin*

Trivial
-------

### Sous dossier


Les fichiers concernant toute l'application sont dans le sous répertoire *2510crowndsourcing/crowndsourcing*.

Pour les fichiers concernant les services backend :

*2510crowndsourcing/server*

### Configuration

Chaque services disposent de son sous-dossier avec son fichier de configuration.

Unique exception le server apache pour l'api est configuré avec le fichier 000-default.conf.

Pour information, toutes modifications faites à l'application et aux différents serveurs nécessite de relancer tous les containers impactés. Le plus simple et de fermer le cluster et le relancer avec les commandes suivantes :

*docker-compose down -f "chemin/vers/docker-compose.yaml"*

*docker-compose up -f "chemin/vers/docker-compose.yaml" --build*

### CI/CD

Le repository git dispose d'un pipeline de CI/CD avec gitlab-ci.yaml qui génère à chaque commit dans la branche dev et main une image docker des services pour s'assurer que l'application est toujours exécutable.

### Base de données

Pour réinitialiser la base de données, il faut importer manuellement le script SQL *bd_crowdsourcing.sql* se trouvant dans le répertoire *2510crowndsourcing/server/MySql* dans phpmyadmin sur [http://localhost:8000](http://localhost:8000/).

Username : user

Password : test

Pour changer les identifiants de connexion, il faut modifier les informations contenues dans le docker-compose.

Troubleshooting
---------------

Si vous avez besoin de plus d'information voici des liens complémentaires

[pour git](https://www.linode.com/docs/guides/how-to-install-git-on-linux-mac-and-windows/)

[toujour pour git](https://docs.microsoft.com/en-us/windows/wsl/tutorials/wsl-git)

[Pour docker](https://docs.docker.com/desktop/windows/wsl/)

[Docker-compose](https://docs.docker.com/compose/install/)
