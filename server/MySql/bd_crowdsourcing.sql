-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : ven. 18 fév. 2022 à 12:18
-- Version du serveur : 5.7.36
-- Version de PHP : 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `bd_crowdsourcing`
--
-- --------------------------------------------------------
--
-- Structure de la table `t_phase`
--

DROP TABLE IF EXISTS `t_phase`;
CREATE TABLE IF NOT EXISTS `t_phase` (
  `phase_id` int(11) NOT NULL AUTO_INCREMENT,
  `phase_numero` int(11) NOT NULL,
  `type_phase_id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL,
  PRIMARY KEY (`phase_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------
--
-- Structure de la table `t_moyenne`
--

DROP TABLE IF EXISTS `t_moyenne`;
CREATE TABLE IF NOT EXISTS `t_moyenne` (
  `moyenne_id` int(11) NOT NULL AUTO_INCREMENT,
  `moyenne_note` FLOAT(1) NOT NULL,
  `idea_id` int(11) NOT NULL,
  PRIMARY KEY (`moyenne_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------

--
-- Structure de la table `t_note`
--

DROP TABLE IF EXISTS `t_note`;
CREATE TABLE IF NOT EXISTS `t_note` (
  `note_id` int(11) NOT NULL AUTO_INCREMENT,
  `note_note` FLOAT(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `idea_id` int(11) NOT NULL,
  PRIMARY KEY (`note_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------
--
-- Structure de la table `t_idea`
--

DROP TABLE IF EXISTS `t_idea`;
CREATE TABLE IF NOT EXISTS `t_idea` (
  `idea_id` int(11) NOT NULL AUTO_INCREMENT,
  `idea_titre` varchar(255) NOT NULL,
  `idea_desc` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`idea_id`),
  KEY `fk_user_id_idea` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `t_user`
--

DROP TABLE IF EXISTS `t_user`;
CREATE TABLE IF NOT EXISTS `t_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_pseudo` varchar(255) NOT NULL,
  `user_mail` varchar(255) NOT NULL,
  `session_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `fk_session_id_user` (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Structure de la table `t_session`
--

DROP TABLE IF EXISTS `t_session`;
CREATE TABLE IF NOT EXISTS `t_session` (
  `session_id` int(11) NOT NULL AUTO_INCREMENT,
  `session_name` varchar(255) NOT NULL,
  `session_date_debut` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `session_date_fin` datetime DEFAULT NULL,
  `session_objet` varchar(255) NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------
--
-- Structure de la table `t_type_phase`
--

DROP TABLE IF EXISTS `t_type_phase`;
CREATE TABLE IF NOT EXISTS `t_type_phase` (
  `type_phase_id` int(11) NOT NULL ,
  `type_phase_libelle` varchar(255) NOT NULL,
  PRIMARY KEY (`type_phase_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------

Insert into `t_type_phase` (`type_phase_id`, `type_phase_libelle`) VALUES
(1,'creation'),
(2,'evaluation'),
(3,'resultat');

-- --------------------------------------------------------
--
-- Structure de la table `t_statut`
--

DROP TABLE IF EXISTS `t_statut`;
CREATE TABLE IF NOT EXISTS `t_statut` (
  `statut_id` int(11) NOT NULL ,
  `statut_libelle` varchar(255) NOT NULL,
  PRIMARY KEY (`statut_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------

Insert into `t_statut` (`statut_id`, `statut_libelle`) VALUES
(1,'facilitateur'),
(2,'participant');

 
--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `t_idea`
--
ALTER TABLE `t_idea`
  ADD CONSTRAINT `fk_user_id_idea` FOREIGN KEY (`user_id`) REFERENCES `t_user` (`user_id`);

--
-- Contraintes pour la table `t_user`
--
ALTER TABLE `t_user`
  ADD CONSTRAINT `fk_session_id_user` FOREIGN KEY (`session_id`) REFERENCES `t_session` (`session_id`);
  
--
-- Contraintes pour la table `t_note`
--
ALTER TABLE `t_note`
  ADD CONSTRAINT `fk_user_id_note` FOREIGN KEY (`user_id`) REFERENCES `t_user` (`user_id`);
ALTER TABLE `t_note`
  ADD CONSTRAINT `fk_idea_id_note` FOREIGN KEY (`idea_id`) REFERENCES `t_idea` (`idea_id`);

  
 --
-- Contraintes pour la table `t_moyenne`
--
ALTER TABLE `t_moyenne`
  ADD CONSTRAINT `fk_idea_id_moyenne` FOREIGN KEY (`idea_id`) REFERENCES `t_idea` (`idea_id`);

 --
-- Contraintes pour la table `t_phase`
--
ALTER TABLE `t_phase`
  ADD CONSTRAINT `fk_session_id_phase` FOREIGN KEY (`session_id`) REFERENCES `t_session` (`session_id`);
ALTER TABLE `t_phase`
  ADD CONSTRAINT `fk_type_phase_id_phase` FOREIGN KEY (`type_phase_id`) REFERENCES `t_type_phase` (`type_phase_id`);

COMMIT;


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
