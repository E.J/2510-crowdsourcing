<?php


if($_SERVER['REQUEST_METHOD'] == 'GET'){

    include_once '../config/database.php';
    include_once '../objects/note.php';

    // On instancie la base de données
    $database = new Database();
    $db = $database->getConnection();

    // On instancie les produits
    $note = new Note($db);

    // On récupère les données
    $stmt = $note->read();

    // On vérifie si on a au moins 1 produit
    if($stmt->rowCount() > 0){
        // On initialise un tableau associatif
        $note_arr = [];

        // On parcourt les produits
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $note = [
                "note_id" => $row['note_id'],
                "note_note" => $row['note_note'],
                "user_id" => $row['user_id'],
                "idea_id" => $row['idea_id']
            ];

            $notes_arr[] = $note;
        }
        // On envoie le code réponse 200 OK
        http_response_code(200);

        // On encode en json et on envoie
        echo json_encode($notes_arr);
    }
}else{
    http_response_code(405);
    echo json_encode(["message" => "La méthode n'est pas autorisée"]);
}
