<?php


if($_SERVER['REQUEST_METHOD'] == 'GET'){

    include_once '../config/database.php';
    include_once '../objects/session.php';

    // On instancie la base de données
    $database = new Database();
    $db = $database->getConnection();

    // On instancie les produits
    $session = new Session($db);

    // On récupère les données
    $stmt = $session->read();

    // On vérifie si on a au moins 1 produit
    if($stmt->rowCount() > 0){
        // On initialise un tableau associatif
        $sessions_arr = [];

        // On parcourt les produits
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $session = [
                "session_id" => $row['session_id'],
                "session_name" => $row['session_name'],
                "session_objet" => $row['session_objet'],
                "session_date_debut" => $row['session_date_debut'],
                "session_date_fin" => $row['session_date_fin']
            ];

            $sessions_arr[] = $session;
        }
        // On envoie le code réponse 200 OK
        http_response_code(200);

        // On encode en json et on envoie
        echo json_encode($sessions_arr);
    }
}else{
    http_response_code(405);
    echo json_encode(["message" => "La méthode n'est pas autorisée"]);
}
