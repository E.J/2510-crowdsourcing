<?php


if($_SERVER['REQUEST_METHOD'] == 'GET'){

    include_once '../config/database.php';
    include_once '../objects/idea.php';

    // On instancie la base de données
    $database = new Database();
    $db = $database->getConnection();

    // On instancie les produits
    $idea = new Idea($db);

    // On récupère les données
    $stmt = $idea->read();

    // On vérifie si on a au moins 1 produit
    if($stmt->rowCount() > 0){
        // On initialise un tableau associatif
        $ideas_arr = [];

        // On parcourt les produits
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $idea = [
                "idea_id" => $row['idea_id'],
                "idea_titre" => $row['idea_titre'],
                "idea_desc" => $row['idea_desc'],
                "user_id" => $row['user_id']
            ];

            $ideas_arr[] = $idea;
        }
        // On envoie le code réponse 200 OK
        http_response_code(200);

        // On encode en json et on envoie
        echo json_encode($ideas_arr);
    }
}else{
    http_response_code(405);
    echo json_encode(["message" => "La méthode n'est pas autorisée"]);
}
