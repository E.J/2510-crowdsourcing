<?php


if($_SERVER['REQUEST_METHOD'] == 'GET'){

    include_once '../config/database.php';
    include_once '../objects/moyenne.php';

    // On instancie la base de données
    $database = new Database();
    $db = $database->getConnection();

    // On instancie les produits
    $moyenne = new Moyenne($db);

        // On set l'id de l'enregistrement à lire
        $moyenne->id = isset($_GET['moyenne_id']) ? $_GET['moyenne_id'] : die();

    // On récupère les données
    $stmt = $moyenne->read();

    // On vérifie si on a au moins 1 produit
    if($stmt->rowCount() > 0){
        // On initialise un tableau associatif
        $moyennes_arr = [];

        // On parcourt les produits
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $moyenne = [
                "moyenne_id" => $row['moyenne_id'],
                "idea_titre" => $row['idea_titre'],
                "idea_desc" => $row['idea_desc'],
                "moyenne_note" => $row['moyenne_note']
            ];

            $moyennes_arr[] = $moyenne;
        }
        // On envoie le code réponse 200 OK
        http_response_code(200);

        // On encode en json et on envoie
        echo json_encode($moyennes_arr);
    }
}else{
    http_response_code(405);
    echo json_encode(["message" => "La méthode n'est pas autorisée"]);
}
