<?php


if($_SERVER['REQUEST_METHOD'] == 'GET'){
    include_once '../config/database.php';
    include_once '../objects/phase.php';

    // On instancie la base de données
    $database = new Database();
    $db = $database->getConnection();

    // On instancie les produits
    $phase = new Phase($db);

        // On set l'id de l'enregistrement à lire
        $phase->id = isset($_GET['phase_id']) ? $_GET['phase_id'] : die();

    // On récupère les données
    $stmt = $phase->read();

    // On vérifie si on a au moins 1 produit
    if($stmt->rowCount() > 0){
        // On initialise un tableau associatif
        $phases_arr = [];
        // On parcourt les produits
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $phase = [
                "phase_numero" => $row['phase_numero'],
                "type_phase_id" => $row['type_phase_id'],
                "session_id" => $row['session_id']
            ];

            $phases_arr[] = $phase;
        }
        // On envoie le code réponse 200 OK
        http_response_code(200);

        // On encode en json et on envoie
        echo json_encode($phases_arr);
    }
}else{
    http_response_code(405);
    echo json_encode(["message" => "La méthode n'est pas autorisée"]);
}
