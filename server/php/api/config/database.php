<?php class Database{
    private $host = "db";
    private $db_name = "bd_crowdsourcing";
    private $username = "root";
    private $password = "test";
    public $connexion;

    public function getConnection(){
        $this->connexion = null;
        try{
            $this->connexion = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
            $this->connexion->exec("set names utf8");
        }catch(PDOException $exception){
            echo "Erreur de connexion : " . $exception->getMessage();
        }
        return $this->connexion;
    }
}