<?php


// On vérifie que la méthode utilisée est correcte
if($_SERVER['REQUEST_METHOD'] == 'GET'){
    // On inclut les fichiers de configuration et d'accès aux données
    include_once '../config/database.php';
    include_once '../objects/user.php';

    // On instancie la base de données
    $database = new Database();
    $db = $database->getConnection();

    // On instancie les produits
    $user = new User($db);

    // On set l'id de l'enregistrement à lire
    $user->id = isset($_GET['user_id']) ? $_GET['user_id'] : die();

    // On récupère le produit
    $user->readOnebyName();
    if($user->pseudo != null){
        $user_arr = [
            "user_id" => $user->id,
            "pseudo" => $user->pseudo,
            "mail" => $user->mail,
            "session_id" => $user->session_id
        ];
    
        // On envoie le code réponse 200 OK
        http_response_code(200);

        // On encode en json et on envoie
        echo json_encode([$user_arr]);
    }else{
        // 404 Not found
        http_response_code(404);
         
        echo json_encode(array("message" => "L'utilisateur n'existe pas."));
    }  
}else{
    // On gère l'erreur
    http_response_code(405);
    echo json_encode(["message" => "La méthode n'est pas autorisée"]);
}