<?php class Session{
    private $connexion;
    private $table = "t_session";

    public $id;
    public $name;
    public $objet;
    public $date_debut;
    public $date_fin;

    public function __construct($db){
        $this->connexion = $db;
    }

    public function read(){
        $sql = "SELECT session_id, session_name, session_objet, session_date_debut, session_date_fin FROM " . $this->table;
        $query = $this->connexion->prepare($sql);
        $query->execute();
        return $query;
    }


    public function create(){
        $sql = "INSERT INTO " . $this->table . " SET session_name=:name, session_objet=:objet";
        $query = $this->connexion->prepare($sql);
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->objet=htmlspecialchars(strip_tags($this->objet));
        $this->id=htmlspecialchars(strip_tags($this->id));
        $query->bindParam(":name", $this->name);
        $query->bindParam(":objet", $this->objet);
        if($query->execute()){
            return true;
        }
        return false;
    }

    public function readOne(){
        // On écrit la requête
        $sql = "SELECT session_id, session_name, session_objet, session_date_debut, session_date_fin FROM " . $this->table . " WHERE session_id= ? LIMIT 0,1";
        // On prépare la requête
        $query = $this->connexion->prepare( $sql );
        // On attache l'id
        $query->bindParam(1, $this->id);
        // On exécute la requête
        $query->execute();
        // on récupère la ligne
        $row = $query->fetch(PDO::FETCH_ASSOC);
        // On hydrate l'objet
        $this->id = $row['session_id'];
        $this->name = $row['session_name'];
        $this->objet = $row['session_objet'];
        $this->date_debut = $row['session_date_debut'];
        $this->date_fin = $row['session_date_fin'];
    }

    public function readOnebyName(){
        // On écrit la requête
        $sql = "SELECT session_id, session_name, session_objet, session_date_debut, session_date_fin FROM " . $this->table . " WHERE session_name= ? LIMIT 0,1";
        // On prépare la requête
        $query = $this->connexion->prepare( $sql );
        // On attache l'id
        $query->bindParam(1, $this->id);
        // On exécute la requête
        $query->execute();
        // on récupère la ligne
        $row = $query->fetch(PDO::FETCH_ASSOC);
        // On hydrate l'objet
        $this->id = $row['session_id'];
        $this->name = $row['session_name'];
        $this->objet = $row['session_objet'];
        $this->date_debut = $row['session_date_debut'];
        $this->date_fin = $row['session_date_fin'];
    }

    public function update(){
        $sql = "UPDATE " . $this->table . " SET session_objet=:objet, session_name=:name, session_date_debut=:date_debut, session_date_fin=:date_fin WHERE session_id = :id";
        $query = $this->connexion->prepare($sql);
        $this->id=htmlspecialchars(strip_tags($this->id));
        $this->objet=htmlspecialchars(strip_tags($this->objet));
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->date_debut=htmlspecialchars(strip_tags($this->date_debut));
        $this->date_fin=htmlspecialchars(strip_tags($this->date_fin));
        $query->bindParam(":id", $this->id);
        $query->bindParam(":objet", $this->objet);
        $query->bindParam(":name", $this->name);
        $query->bindParam(":date_debut", $this->date_debut);
        $query->bindParam(":date_fin", $this->date_fin);
        if($query->execute()){
            return true;
        }
        return false;
    }

    public function delete(){
        $sql = "DELETE FROM " . $this->table . " WHERE session_id = ?";
        $query = $this->connexion->prepare($sql);
        $this->id=htmlspecialchars(strip_tags($this->id));
        $query->bindParam(1, $this->id);
        if($query->execute()){
            return true;
        }
        return false;
    }
}