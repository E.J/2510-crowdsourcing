import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LottiePlayer } from "@lottiefiles/lottie-player";


import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { BoardComponent } from './board/board.component';
import { MenuComponent } from './menu/menu.component';
import { InstructionsComponent } from './instructions/instructions.component';
import { ChatComponent } from './chat/chat.component';
import { RouterModule, Routes } from '@angular/router';
import { CreateSessionComponent } from './create-session/create-session.component';
import { WaitingRoomComponent } from './waiting-room/waiting-room.component';
import { SettingsSessionComponent } from './settings-session/settings-session.component';
import { ResultBoardComponent } from './result-board/result-board.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { ErrorNotFoundComponent } from './error-not-found/error-not-found.component';

const urlAPI="localhost";

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'connexion', component: LoginComponent },
  { path: 'accueil', component: BoardComponent },
  { path: 'consignes', component: InstructionsComponent },
  { path: 'configuration', component: SettingsSessionComponent },
  { path: 'waitingRoom', component: WaitingRoomComponent },
  { path: 'resultats', component: ResultBoardComponent },
  { path: 'dashboard', component: AdminDashboardComponent },
  { path: '**', component: ErrorNotFoundComponent },
]

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    BoardComponent,
    MenuComponent,
    InstructionsComponent,
    ChatComponent,
    CreateSessionComponent,
    WaitingRoomComponent,
    SettingsSessionComponent,
    ResultBoardComponent,
    AdminDashboardComponent,
    ErrorNotFoundComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  providers: [ { provide: 'URL', useValue: urlAPI}],
  bootstrap: [AppComponent]
})


export class AppModule {

  ngOnInit(): void {

  }

  ngOnDestroy(): void {

  }
 }
