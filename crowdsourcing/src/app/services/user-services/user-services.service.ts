import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { NewUser } from 'src/app/models/newuser.model';

@Injectable({
  providedIn: 'root'
})
export class UserServicesService {

  users: User[] = [];
  user: User[] = [];
  newusers: NewUser[] = [];
  newuser: NewUser[] = [];
  constructor(private httpClient: HttpClient, private injector: Injector) { }
  urlAPI = this.injector.get('URL');

  async addNewUser(newuser: NewUser): Promise<void> {
    await this.newusers.push(newuser);
    await this.saveNewUserOnServer(newuser);
  }


  saveNewUserOnServer(newuser: NewUser): void {
    this.httpClient.post("http://localhost:8001/api/user/create.php", JSON.stringify(newuser)).subscribe(
      () => {
        console.log("L'utilisateur a bien été créé.");
      },
      (error) => {
        console.log("Erreur " + JSON.stringify(error) + ". L'utilisateur n'a pas pu être créé.");
      }
    );
  }

  getLastEntry(): Observable<User[]> {
    return this.httpClient.get<User[]>("http://localhost:8001/api/user/readLastEntry.php");
  }

  getReadUserbyName(user_name: string): Observable<User[]> {
    let parametres = new HttpParams().set('user_id', user_name);
    return this.httpClient.get<User[]>("http://localhost:8001/api/user/readOnebyName.php?" + parametres.toString());
  }

  getUsers(sessionID:string): Observable<User[]> {
    let parametres = new HttpParams().set('user_id', sessionID);
    return this.httpClient.get<User[]>("http://localhost:8001/api/user/read.php?"+ parametres.toString());
  }
}
