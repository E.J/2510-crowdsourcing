import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject, throwError } from 'rxjs';
import { catchError, count, retry } from 'rxjs/operators';
import { User } from 'src/app/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  users: User[] = [];
  usersSub = new Subject<User[]>();
  user = [];


  constructor(private httpClient: HttpClient) {
    this.getUsers();
   }

  emitUsers(): void {
    this.usersSub.next(this.users);
  }

  addNewUser(user: User): void {
    this.users.push(user);
    this.createUser(user);
  }

  createUser(user: User): void {
    this.httpClient.post("http://localhost:8001/api/user/create.php",JSON.stringify(user)).subscribe(
      () => {
        console.log("L'utilisateur a bien été enregistrée.");
      },
      (error) => {
        console.log("Erreur " + error + ". L'utilisateur n'a pas pu être enregistrée.");
      }
    );
  }

  getUsers(): void {
    this.httpClient.get<User[]>("http://localhost:8001/api/user/read.php")
      .subscribe(
        (usersData= []) => {
          if (usersData == null) {
            this.users = [];
          } else {
            this.users = usersData;
          }
          this.emitUsers();
        },
        (error) => {
          console.log("Erreur " + error + ". Impossible de récupérer les données.");
        },
        () => {
          console.log("Récupération des données terminée !");
        }
      );
  }




}


