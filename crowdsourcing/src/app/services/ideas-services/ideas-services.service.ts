import { HttpClient, HttpHeaders, HttpClientModule, HttpParams } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { inject } from '@angular/core/testing';
import { Observable, Subject, from } from 'rxjs';

import { Idea } from 'src/app/models/idea.model';



@Injectable({
  providedIn: 'root'
})

export class IdeasServicesService {

  ideas: Idea[] = [];
  ideasSub = new Subject<Idea[]>();
  nb: Number = 0;
  nbMax!: number;
  idea = [];
  randomIdeaIndex!: number;

  constructor(private httpClient: HttpClient, private injector: Injector) {
    this.getIdeasFromServer();
  }

  urlAPI = this.injector.get('URL');

  emitIdeas(): void {
    this.ideasSub.next(this.ideas);
  }

  addNewIdea(idea: Idea): void {
    this.ideas.push(idea);
    //this.emitIdeas();
    this.saveNewIdeaOnServer(idea);
  }


  saveNewIdeaOnServer(idea: Idea): void {
    this.httpClient.post("http://localhost:8001/api/idea/create.php", JSON.stringify(idea)).subscribe(
      () => {
        console.log("L'idée a bien été enregistrée.");
      },
      (error) => {
        console.log("Erreur " + JSON.stringify(error) + ". L'idée n'a pas pu être enregistrée.");
      }
    );
  }

  getIdeasFromServer(): void {
    this.httpClient.get<Idea[]>("http://localhost:8001/api/idea/read.php")
      .subscribe(
        (ideasData = []) => {
          if (ideasData == null) {
            this.ideas = [];
          } else {
            this.ideas = ideasData;
          }
          this.emitIdeas();
        },
        (error) => {
          console.log("Erreur " + error + ". Impossible de récupérer les données.");
        },
        () => {
          console.log("Récupération des données terminée !");
        }
      );
  }

  getIdeas(): Observable<Idea[]> {
    return this.httpClient.get<Idea[]>("http://localhost:8001/api/idea/read.php");
  }

  getRandomIdeaFromServer(user_id: number, session_id: number): Observable<Idea[]> {
    let parametres = new HttpParams();
    parametres = parametres.append('user_id', user_id);
    parametres = parametres.append('session_id', session_id);
    return this.httpClient.get<Idea[]>("http://localhost:8001/api/idea/readOneRandomExcludID.php?" + parametres.toString());
  }
}
