import { HttpClient, HttpHeaders, HttpClientModule, HttpParams } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Observable, Subject, from } from 'rxjs';
import { map } from 'rxjs/operators';

import { Note } from 'src/app/models/note.model';



@Injectable({
  providedIn: 'root'
})

export class NotesServicesService {

  notes: Note[] = [];
  notesSub = new Subject<Note[]>();
  note = [];

  constructor(private httpClient: HttpClient, private injector:Injector) { 
  }

  urlAPI=this.injector.get('URL');
  
  addNewNote(note: Note): void {
    this.notes.push(note);
    this.saveNewNoteOnServer(note);
  }


  saveNewNoteOnServer(note: Note): void {
    this.httpClient.post("http://localhost:8001/api/note/create.php",JSON.stringify(note)).subscribe(
      () => {
        console.log("La note a bien été enregistrée.");
      },
      (error) => {
        console.log("Erreur " + error + ". La note n'a pas pu être enregistrée.");
      }
    );
  }
}



