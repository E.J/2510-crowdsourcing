import { TestBed } from '@angular/core/testing';

import { GetDataSessionService } from './get-data-session.service';

describe('GetDataSessionService', () => {
  let service: GetDataSessionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GetDataSessionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
