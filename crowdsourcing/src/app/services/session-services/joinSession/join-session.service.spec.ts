import { TestBed } from '@angular/core/testing';

import { JoinSessionService } from './join-session.service';

describe('JoinSessionService', () => {
  let service: JoinSessionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JoinSessionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
