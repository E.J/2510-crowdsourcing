import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsSessionComponent } from './settings-session.component';

describe('SettingsSessionComponent', () => {
  let component: SettingsSessionComponent;
  let fixture: ComponentFixture<SettingsSessionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingsSessionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsSessionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
