import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PhasesServicesService } from '../services/phases-services/phases-services.service';

@Component({
  selector: 'app-waiting-room',
  templateUrl: './waiting-room.component.html',
  styleUrls: ['./waiting-room.component.css']
})
export class WaitingRoomComponent implements OnInit {

  sessionID:any;
  interval:any;

  constructor(private phasesService:PhasesServicesService, private router: Router) { }

  ngOnInit(): void {
    this.sessionID=sessionStorage.getItem('sessionID');
    this.interval= setInterval(() => {this.listenPhase(this.sessionID)}, 3000);

  }

  listenPhase(sessionID:string){
    this.phasesService.getReadPhase(sessionID).subscribe(
      res=>{
        if((res!=null)&&(res[0].type_phase_id==1)&&(res[0].phase_numero==1)){
          clearInterval(this.interval);
          this.router.navigate(["accueil"], { skipLocationChange: true });

        }
      }
    );
  }

}
