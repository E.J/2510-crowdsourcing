import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { User } from '../models/user.model';
import { UserServicesService } from '../services/user-services/user-services.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  @ViewChild('menu') input: any;
  users!: User[];
  public uniqueUser: any;
  sessionID:any;
  wait:any;
  interval:any;

  constructor(private elRef: ElementRef, private userService: UserServicesService) { }

  ngOnInit(): void {
    this.uniqueUser = String(sessionStorage.getItem('userPseudo'));
    this.sessionID = sessionStorage.getItem('sessionID');
    this.wait= sessionStorage.getItem('wait');
    this.interval= setInterval(() => {this.getUsersList(),
      this.wait= sessionStorage.getItem('wait');
    }, 3000);
   // this.getUsersList();
  }

  ngAfterViewInit() {
    var toggle = this.elRef.nativeElement.querySelector('.toggle');
    var menu = this.elRef.nativeElement.querySelector('.menu');
    toggle.onclick = function () {
      menu.classList.toggle('active')
    }
  }

  getUsersList() {
    if(this.wait==1){
      console.log('ecoute');
      this.userService.getUsers(this.sessionID).subscribe(
        (getUsers: User[]) => {
          this.users = getUsers;
          console.log(getUsers);
        }
      )}
    else{
      clearInterval(this.interval);
      console.log('stop');
      this.userService.getUsers(this.sessionID).subscribe(
        (getUsers: User[]) => {
          this.users = getUsers;
          console.log(getUsers);
        }
      )
    }  
  }
}
