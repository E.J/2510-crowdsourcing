import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Session } from '../models/session.model';
import { NewSession } from '../models/newsession.model';
import { User } from '../models/user.model';
import { NewUser } from '../models/newuser.model';
import { CreateSessionService } from '../services/session-services/createSession/create-session.service';
import { UserServicesService } from '../services/user-services/user-services.service';
import { GetDataSessionService } from '../services/session-services/getDataSession/get-data-session.service';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  public loginButtonHiddenStatus: any;
  public hiddenStatusLogInGif: any;
  public createSessionButtonHiddenStatus: any;
  public hiddenStatusCreateSessionGif: any;
  createNewSession!: FormGroup;
  joinSession!: FormGroup;
  session!: Session;
  newsession!: NewSession;
  sessionID: any;
  sessionObjet: any;
  sessionName: any;
  newuser!: NewUser;
  user!: User;
  userID: any;
  userPseudo: any;
  userStatut:any;
  timer: number = 3000;
  timer2:number=4000;

  @ViewChild('joinSessionForm') input: any;


  constructor(private elRef: ElementRef, private formBuilder: FormBuilder, private userService: UserServicesService,
    private createSessionService: CreateSessionService, private dataSessionService: GetDataSessionService, private router: Router) { }

  ngOnInit(): void {
    this.loginButtonHiddenStatus = false;
    this.hiddenStatusLogInGif = true;
    this.createSessionButtonHiddenStatus = false;
    this.hiddenStatusCreateSessionGif = true;
    this.initCreateNewSessionForm();
    this.initJoinSessionForm();
  }

  initCreateNewSessionForm() {
    this.createNewSession = this.formBuilder.group({
      user_pseudo: this.formBuilder.control("", [Validators.required, Validators.minLength(3)]),
      session_objet: this.formBuilder.control("", [Validators.required, Validators.minLength(3)])
    });
  }

  initJoinSessionForm() {
    this.joinSession = this.formBuilder.group({
      user_pseudo: this.formBuilder.control("", [Validators.required, Validators.minLength(3)]),
      session_name: this.formBuilder.control("", [Validators.required, Validators.minLength(3)])
    });
  }

  ngAfterViewInit() {
    const loginForm = this.elRef.nativeElement.querySelector("form.login");
    const signupForm = this.elRef.nativeElement.querySelector("form.signup");
    const loginBtn = this.elRef.nativeElement.querySelector("label.login");
    const signupBtn = this.elRef.nativeElement.querySelector("label.signup");
    const signupLink = this.elRef.nativeElement.querySelector(".signup-link a");
    const loginText = this.elRef.nativeElement.querySelector(".title-text .login");
    const signupText = this.elRef.nativeElement.querySelector(".title-text .signup");



    signupBtn.onclick = (() => {
      loginForm.style.marginLeft = "-50%";
      loginText.style.marginLeft = "-50%";
    });

    loginBtn.onclick = (() => {
      loginForm.style.marginLeft = "0%";
      loginText.style.marginLeft = "0%";
    });
  }

  /**
   * Permet de créer une nouvelle session dans la table 't_session'
   */
  async createSession() {

    this.createSessionButtonHiddenStatus = true;
    this.hiddenStatusCreateSessionGif = false;

    const dataNewSession = this.createNewSession.value;
    let sessionName = this.createSessionService.createRandomSessionName();
    console.log('Test sessionName : ' + sessionName);

    /**
     * Définition des constantes newSession nécéssaires à la création d'une nouvelle session
     * newSession récupère les données des champs "Nom de la session" (= session_name) et "Problématique" (= session_objet)
     */
    const newSession = new NewSession(sessionName, "NULL", dataNewSession.session_objet);

    /**
     * Appelle le service CreateSessionService
     * pour créer la nouvelle session
     */
    await this.createSessionService.addNewSession(newSession);

    setTimeout(() => {
      //console.log('nom' +sessionName)
      sessionStorage.setItem('wait','1');
      this.createUser(sessionName, dataNewSession.user_pseudo);
    },this.timer);

    /**
    * Permet d'envoyer l'utilisateur sur la page "waiting-room"
    */
     setTimeout(()=>{
      this.router.navigate(["dashboard"], { skipLocationChange: true });
    },this.timer2);

  }

  async createUser(session_name: string, user_pseudo: string) {
    const user_mail = "mail.test@gmail.com";
    await this.createSessionService.getReadSessionbyName(session_name).subscribe(res => {
      this.sessionID = res[0].session_id;
      this.sessionObjet = res[0].session_objet;
      this.sessionName = res[0].session_name;
      sessionStorage.setItem('sessionID', this.sessionID);
      sessionStorage.setItem('sessionObjet', this.sessionObjet);
      sessionStorage.setItem('sessionName', this.sessionName);
      const newuser = new NewUser(user_pseudo, user_mail, this.sessionID);
      this.userService.addNewUser(newuser);
      sessionStorage.setItem('userPseudo', user_pseudo);
      setTimeout(() => {
        this.userService.getReadUserbyName(user_pseudo).subscribe(result => {
          this.userID = result[0].user_id;
          sessionStorage.setItem('userID', this.userID);
          console.log('pseudo : '+ sessionStorage.getItem('userPseudo'));
          console.log('user id : '+ sessionStorage.getItem('userID'));
          console.log('session id : '+ sessionStorage.getItem('sessionID'));
          console.log('session objet : '+ sessionStorage.getItem('sessionObjet'));
          console.log('session name : '+ sessionStorage.getItem('sessionName'));
        })
      }, this.timer);
    })
  }

  /**
   * Permet de créer une nouvelle session dans la table 't_session'
   */
  async joinSessionByIdSession() {

    this.loginButtonHiddenStatus = true;
    this.hiddenStatusLogInGif = false;

    /**
     * Récupère les infos saisies par l'utilisateur
     */
    let dataSession = this.joinSession.value;

    /**
     * Vérifie que la session correspondant à l'id existe bien.
     */
    try {
      await this.dataSessionService.getSessionFromServer(dataSession.session_name).subscribe(res => {
        setTimeout(() => {
          this.session = res[0];
          this.sessionID = this.session.session_name;
          this.sessionName = this.session.session_name;

          if (this.sessionID == dataSession.session_name) {
            this.createUser(this.sessionName, dataSession.user_pseudo);
          }
        }, this.timer);
      });

      /**
       * Permet d'envoyer l'utilisateur sur la page "waiting-room"
       */
      setTimeout(()=>{
        this.router.navigate(["waitingRoom"], { skipLocationChange: true });
      },this.timer2);

    } catch (error) {
      console.log('Erreur : ' + JSON.stringify(error));
    }
  }
}
