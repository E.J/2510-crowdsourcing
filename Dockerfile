# Stage 1: Compile and Build angular codebase

# Use official node image as the base image
FROM node:latest as build-step
ENV NODE_OPTIONS=--openssl-legacy-provider

# Set the working directory
RUN mkdir -p /usr/local/app/
WORKDIR /usr/local/app/

# Add the source code to app
COPY ./crowdsourcing/ /usr/local/app/

# Install all the dependencies
RUN npm install

RUN npm install --save @types/node

# Generate the build of the application
RUN npm run build

# Stage 2: Serve app with nginx server

# Use official nginx image as the base image
FROM nginx:latest

# Copy the build output to replace the default nginx contents.
COPY ./server/nginx/nginx.conf /etc/nginx/nginx.conf
COPY --from=build-step /usr/local/app/dist/crowdsourcing /usr/share/nginx/html

# Expose port 80
EXPOSE 80